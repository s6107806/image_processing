import numpy as np
from matplotlib import pylab as plb
from matplotlib import pyplot as plt
def read_image(file_name):  
    image_data=plb.imread(file_name)
    return image_data

def show_properties(image_data): 
    print("Height of image is {}".format(image_data.shape[0]))
    print("Widht of image is {}".format(image_data.shape[1]))
    print("Dimension of image is {}".format(image_data.ndim))
    print("Size of image is {}".format(image_data.size))
def show_image(image_data):
    plt.imshow(image_data,cmap='gray')
    plt.show()
def to_grayscale(image_data):
    gray_scale = np.ndarray(shape=image_data.shape[:2],dtype=np.uint8)
    #for i in range(image_data.shape[0]):
        #for j in range(image_data.shape[1]):
           # gray_scale[i,j]= 0.33*image_data[i,j,0]+0.33*image_data[i,j,1]+0.33*image_data[i,j,2]
    gray_scale= 0.2126*image_data[1,1,0]+0.7152*image_data[1,1,1]+0.0223*image_data[1,1,2]
    return gray_scale